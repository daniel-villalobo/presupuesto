import React, { useState } from "react";
import "./App.css";
import "./bootstrap.min.css";
import Titulo from "./components/Titulo";
import Pregunta from "./components/Pregunta";
import Formulario from "./components/Formulario";
import Listado from "./components/Listado";
import ControlPresupuesto from "./components/ControlPresupuesto";
import Error from "./components/Error";

function App() {
  const [presupuesto, setPresupuesto] = useState(0); //aquí inicializo la variable
  const [gastos, setGastos] = useState([]) //gastos es nombre del state y setGastos es la función para guardar gastos

  return (
    <div className="">
      <div className="my-3">
        <Titulo />
        {/* Formulario para agregar el presuesto */}
        {presupuesto <= 0 ? (
          <div className="container bg-light p-5 my-3">
            <Pregunta guardarPresupuesto={setPresupuesto} />
          </div>
        ) : (
          <div className="container bg-light p-5 my-3">
            <div className="row">
              {/* agrregar datos */}
              <div className="col-sm-12 col-md-6 col-lg-6">
                <div className="container bg-light p-5 my-3">
                  <Formulario />
                </div>
              </div>
              {/* listado */}
              <div className="col-sm-12 col-md-6 col-lg-6">
                <div className="container bg-light p-5 my-3">
                  <Listado />
                </div>
                <div className="container bg-light p-5 my-3">
                  <ControlPresupuesto mostrarPresupuesto={presupuesto}/>
                </div>
              </div>
            </div>
          </div>
        )}

        {/* alert de error */}
        <div className="container my-3">
          <Error />
        </div>
      </div>
    </div>
  );
}

export default App;
