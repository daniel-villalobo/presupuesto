import React from "react";

const ControlPresupuesto = (props) => {
  return (
    <div>
      <div class="alert alert-dismissible alert-primary">
        <p className="mb-0">Presupuesto {props.mostrarPresupuesto}</p>
      </div>
      <div class="alert alert-dismissible alert-success">
        <p className="mb-0">Presupuesto $150</p>
      </div>
    </div>
  );
};

export default ControlPresupuesto;
