import React from 'react';

const Error = () => {
    return (
        <div className="container">
            <div class="alert alert-dismissible alert-warning">
                <p className="mb-0">El presupuesto es incorrecto</p>
            </div>
            
        </div>
    );
};

export default Error;