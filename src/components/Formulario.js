import React from "react";

const Formulario = () => {
  return (
    <div>
      <h3>Agrega tus gastos aquí</h3>
      <form>
        <div class="form-group row">
          <label for="exampleInputEmail1">Nombre gasto</label>
          <input
            type="text"
            class="form-control"
            id=""
            aria-describedby="emailHelp"
            placeholder="Ej.Transporte"
          />
          <label for="exampleInputEmail1">Cantidad de gasto</label>
          <input
            type="text"
            class="form-control"
            id=""
            aria-describedby="emailHelp"
            placeholder="0"
          />
          <button type="button" class="btn btn-success my-2">
            Agregar
          </button>
        </div>
      </form>
    </div>
  );
};

export default Formulario;
