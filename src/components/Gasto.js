import React from "react";

const Gasto = () => {
  return (
    <div>
      <label className="col-sm-10 col-md-10 col-lg-10">Colectivo</label>
      <span className="badge badge-primary col-sm-2 col-md-2 col-lg-2">
        30,00
      </span>

      <label className="col-sm-10 col-md-10 col-lg-10">Super</label>
      <span className="badge badge-primary col-sm-2 col-md-2 col-lg-2">
        350,00
      </span>
    </div>
  );
};

export default Gasto;
