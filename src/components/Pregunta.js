import React, { useState } from "react";

const Pregunta = (props) => {
  const [presupuesto, setPresupuesto] = useState("");

  const handleChange = e => {
    setPresupuesto(parseInt(e.target.value));
  };

  const handleSubmit = e => {
    e.preventDefault();

    //validamos el campo input
    if(presupuesto === '' || presupuesto < 0){
      alert("debe completar todos los datos")
      return;
    }

    //aquí envío el pesupuesto app
    props.guardarPresupuesto(presupuesto)
  }


  return (
    <div>
      <form onSubmit={handleSubmit}>
        <div className="form-group">
          <label className="col-form-label col-form-label-lg">
            Presupuesto
          </label>
          <input
            className="form-control form-control-lg"
            type="text"
            placeholder="Agrega tu presupuesto"
            id="inputLarge"
            onChange={handleChange}
          />
        </div>
        <button type="submit" class="btn btn-success btn-lg btn-block">
          Definir presupuesto
        </button>
      </form>
    </div>
  );
};

export default Pregunta;
